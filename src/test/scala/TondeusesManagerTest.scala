import org.scalatest._
import flatspec._
import org.scalatest.matchers.should.Matchers._

class TondeusesManagerTest extends AnyFlatSpec {

  val testFile = "src/test/resources/resources1.txt"
  val manager = new TondeusesManager()
  manager.initializeDim(testFile)


  "result" should "have value (5,5)" in {
    val result = (manager.supY.get, manager.supY.get)
    assert(result == (5, 5))
  }

  "tondeuses" should "have size 2" in {
    val tondeuses = manager.instantiateTondeuses(testFile)
    assert(tondeuses.size == 2)
  }

  "tondeuses" should "be in these positions" in {
    val tondeuses = manager.instantiateTondeuses(testFile)
    val td1 = tondeuses(0)
    val td2 = tondeuses(1)
    assert(td1.getPosition() == (1, 3))
    assert(td1.getDirection() == 'N')
    assert(td2.getPosition() == (5, 1))
    assert(td2.getDirection() == 'E')
    succeed
  }
}
