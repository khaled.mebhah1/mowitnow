import scala.collection.mutable.ArrayBuffer
//Dans cette classe, un Tondeuse est défini avec des attributs qui déterminent sa position de la forme (x,y) et sa direction
class Tondeuse(var x: Int = 0, var y: Int = 0, var dir: Char = 'N') {
  //Pour les coordonees (x,Y)
  def getPosition() = (this.x, this.y)
    //Pour la direction ( N, S, E ou W)
  def getDirection() = this.dir
    // Cette méthode dicte le mécanisme de déplacement des tondeuses 
  def bouger(inst: Char, forbidenPoints: ArrayBuffer[(Int, Int)], supX: Int, supY: Int): Unit = {
        //>>Inst: Élément possible de l'instruction tel que A, D ou G 
        //>>forbidenPoints: Les points interdits sont définis ici comme toute coordonnée 
        ///égale à la coordonnée à laquelle la deuxième Tondeuse est déjà située ou à 
        ///laquelle l'espace défini pour le plan à partir des instructions est laissé.
        //>>supX: cette valeur enregistre la limite horizontale du champ dans 
        ///lequel les tondeuses vont se déplacer. Au-delà de ce point, comme on
        ///le verra dans la classe "Tondeuse", les machines ne pourront plus avancer.
        //>>supY:cette valeur enregistre la limite verticale du champ dans lequel les 
        ///tondeuses vont se déplacer. Au-delà de ce point, comme on le verra dans la 
        ///classe "Tondeuse", les machines ne pourront plus avancer.
    inst match {
        // Cette instruction demande au code, si l'instruction est celle de A (avancer), 
        ///d'évaluer la direction dans laquelle se trouve la tondeuse et d'ajouter une  
        ///unité d'espace dans cette direction. Dans le resultats "else" où le mouvement souhaité 
        ///positionne la machine dans l'un des points interdits (expliqués ci-dessous), 
        ///la commande devient un "ne pas procéder" et la machine conserve sa position précédente.

        //Pour Avancer
      case 'A' => dir match {
          //Si la tondeuse voit au Nord
        case 'N' =>
          if (notForbiden(x, y + 1, forbidenPoints, supX, supY)) {
            y = y + 1
          }
          else {
            println("Donda N")
            println(x, y)
          }
          //Si la tondeuse voit au Sud
        case 'S' =>
          if (notForbiden(x, y - 1, forbidenPoints, supX, supY)) {
            y = y - 1
          }
          else {
            println("Donda S")
            println(x, y)
          }
          //Si la tondeuse voit a l'Est
        case 'E' =>
          if (notForbiden(x + 1, y, forbidenPoints, supX, supY)) {
            x = x + 1
          }
          else {
            println("Donda E")
            println(x, y)
          }
          //Si la tondeuse voit a l'Ouest
        case 'W' =>
          if (notForbiden(x - 1, y, forbidenPoints, supX, supY)) {
            x = x - 1
          }
          else {
            println("Donda O")
            println(x, y)
          }
      }
        //Ou, dans le cas où l'instruction est de tourner (vers la droite ou vers 
        ///la gauche (D ou G)), la direction actuelle de la tondeuse est 
        ///changée en tenant compte de la direction du virage.

        //Si la tondeuse doit virer a Gauche
      case 'G' => dir match {
        case 'N' => dir = 'W'
        case 'E' => dir = 'N'
        case 'S' => dir = 'E'
        case 'W' => dir = 'S'
      }

      //Si la tondeuse doit virer a Droite
      case 'D' => dir match {
        case 'N' => dir = 'E'
        case 'W' => dir = 'N'
        case 'S' => dir = 'W'
        case 'E' => dir = 'S'
      }
      case _ =>
      //Dans le cas où une lacune ou une instruction non reconnue par 
      ///le programme est rencontrée, l'ordre donné à la tondeuse est 
      ///de se déplacer et de conserver son état précédent.
    }
  }
    // Comme mentionné, les points interdits sont définis ici comme toute coordonnée 
    ///égale à la coordonnée à laquelle la deuxième Tondeuse est déjà située ou à 
    ///laquelle l'espace défini pour le plan à partir des instructions est laissé().
    ///Puisque ce plan cartésien a été conçu avec l'origine à (0,0) et que les 
    ///coordonnées limites sont positives, le résultat ne peut se trouver que dans 
    ///le quadrant I, et donc, les coordonnées résultantes ne peuvent être que positives.
  private def notForbiden(x: Int, y: Int, forbidenPoints: ArrayBuffer[(Int, Int)], supX: Int, supY: Int) = {
    (x >= 0) && (y >= 0) && (x <= supX) && (y <= supY) && !(forbidenPoints contains(x, y))
  }
}
