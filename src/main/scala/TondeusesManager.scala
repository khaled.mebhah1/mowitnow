import scala.collection.mutable.ArrayBuffer
import scala.io.Source
// Cette classe définit le processus d'extraction et d'organisation des
/// informations du document de base, y compris les instructions. 
class TondeusesManager(var supX: Option[Int], var supY: Option[Int]) {
  def this() = this(Option(0), Option(0))
  //>>supX: cette valeur enregistre la limite horizontale du champ dans 
  ///lequel les tondeuses vont se déplacer. Au-delà de ce point, comme on
  ///le verra dans la classe "Tondeuse", les machines ne pourront plus avancer.
  //>>supY:cette valeur enregistre la limite verticale du champ dans lequel les 
  ///tondeuses vont se déplacer. Au-delà de ce point, comme on le verra dans la 
  ///classe "Tondeuse", les machines ne pourront plus avancer.

  //Cette méthode évalue la première ligne du document d'instruction inséré et 
  ///en extrait les coordonnées des limites du terrain.
  def initializeDim(filePath: String): Unit = {

    try {
      val src = Source.fromFile(filePath)
      val dimTerrain = src.getLines().next()
      src.close()
      // Puisque le format de la première ligne doit être (X _ Y), ce qui implique
      /// 3 champs, au cas où il y aurait plus ou moins de champs, le programme 
      ///informera l'utilisateur que le format saisi n'est pas acceptable.
      if (dimTerrain.length() != 3) {
        Console.println("FILE FORMAT NOT COMPATIBLE")
      }
        //S'il s'agit du format correct(si le premier et le troisième champ 
        ///contiennent des digits), le programme définira les limites pour X et Y du
        /// champ. 
      if (Character.isDigit(dimTerrain.charAt(0)) && Character.isDigit(dimTerrain.charAt(2))) {
        this.supX = Option(dimTerrain.charAt(0).asDigit)
        this.supY = Option(dimTerrain.charAt(2).asDigit)
      }
        //Si les données saisies comportent le nombre de champs souhaité mais
        ///contiennent des valeurs non souhaitées, le programme vous informe 
        ///également du format inapproprié.
      else {
        Console.println("FILE FORMAT NOT COMPATIBLE")
      }
    } catch {
      //Si des champs vides sont trouvés, le document sera annoncé comme vide.
      case _:
        NoSuchElementException => println("EMPTY FILE")
    }
  }
  // Cette méthode permet d'interpréter les lignes suivantes du fichier 
  ///d'instructions et de les classer soit comme des coordonnées de position, 
  ///soit comme des instructions. 
  def instantiateTondeuses(filePath: String): ArrayBuffer[Tondeuse] = {

    val tondeuses = ArrayBuffer[Tondeuse]()
    val forbidenPoints = ArrayBuffer[(Int, Int)]()
    
    var i = 1
    var tondeuse = new Tondeuse()
    //chaque "ligne" sera une des lignes du fichier de base (si la ligne était 1
    /// il s'agirait d'une ligne de position initiale).
    for (line <- Source.fromFile(filePath).getLines) {
      if (i != 1) {
        //Pour obtenir uniquement les lignes paires(on commence des 0) : les instructions.
        if (i % 2 == 1) {
          //Créer excecution du déplacement de chaque 
          ///tondeuse à partir du fichier de base, après avoir récupéré
          /// les limites du plan et les points interdits.
          var w:Int=0
          for (instruction <- line) {
            //Pour imprimer la position, à chaque étape, de la tondeuse
            if(((i-1)/2) != 0){println(s"Movement ${w}, Position of mower ${(i-1)/2}: ${tondeuse.getPosition()} , ${tondeuse.getDirection()}")}
            tondeuse.bouger(instruction, forbidenPoints, this.supX.get, this.supY.get)
            w = w + 1
          }
          tondeuses.append(tondeuse)
          //Faire correspondre la position de l'une des Tondesues à 
          ///l'interdiction des autres Tondeuses
          forbidenPoints.append(tondeuse.getPosition())
        }
        //Dans tous les autres cas, une nouvelle instance de la classe Tondeuse
        /// contenant ses attributs de position (X et Y) et son adresse sera créée.
        else {
          tondeuse = new Tondeuse(line.charAt(0).asDigit, line.charAt(2).asDigit, line.charAt(4))
        }

      }
      i = i + 1
    }
    tondeuses
  }
}
