// Le but de cet objet est d'importer dans le document les dimensions du champ à parcourir et les instructions que les tondeuses doivent suivre dans cet espace, y compris leur avance et leur direction.
object Main extends App {
  val testFile = "src/test/resources/resources1.txt"
  //Cette Path peut être remplacée par toute instruction souhaitée
  val manager = new TondeusesManager()
  manager.initializeDim(testFile)
  val tondeuses = manager.instantiateTondeuses(testFile)
 // Pour imprimer la position  des Tondeuses.
  println("Final position of the Lawnmowers:")
  var i = 0
  while (i<tondeuses.size){
    println(s"Mower ${i+1} : ${tondeuses(i).getPosition()} ${tondeuses(i).getDirection()}")
    i += 1
  }
}
