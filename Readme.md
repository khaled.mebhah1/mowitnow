# M2 IMSD : Projet Scala
### Authors :
- Khaled MEBHAH | khaled.mebhah@gmail.com
- Manuel Esteban ARIAS | manuelestebanarias@gmail.com

### How to Run :
Le projet a bien une classe Main pour l'exécuter, 
cependant, nous avons écrit quelques tests afin de tester 
les fonctions primaires du projet et de s'assurer qu'il
qu'il est bien fonctionnel (nous vérifions que les résultats sont corrects 
comme mentionné dans la mission).
